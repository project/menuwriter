<?php
chdir('../../../../../');
require_once 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
$profile = signwriter_load_profile(variable_get('menuwriter_block_L1_standardprofile',''));
$spacer = variable_get('menuwriter_block_spacer','');
$height = $profile->height;
$neg_height = -1*$height;
$width = $profile->width;
$active_position = -2 * $height;
header("Content-Type: text/css");
?>
#menuwriter_block {
  list-style: none outside;
  margin: 0px;
  padding: 0px;
}
#menuwriter_block ul, #menuwriter_block ul ul,#menuwriter_block ul ul ul{
  margin-top:<?php print $spacer ?>px;
}
#menuwriter_block  li {
  list-style: none outside;
  background-image: none;
  padding:0;
  margin-left:0;
  margin-bottom:<?php print $spacer ?>px;
}
#menuwriter_block li a{
  display: block;
  height:<?php print $height ?>px;
  width: <?php print $width ?>px;
  overflow: hidden;
  position: relative ;top: 0px; left: 0px;
}
#menuwriter_block li a.active img{
  position: relative ;top: <?php print $active_position ?>px; left: 0px;
}
#menuwriter_block li.expanded, #menuwriter_block li.collapsed, #menuwriter_block li.leaf {
  list-style-type: none;
  list-style-image: none;
}
/* .over is added to menuwriter links by the jQuery script menuwriter.js when the mouse rolls over them*/
.over{
  position: relative ;top: <?php print $neg_height ?>px; left: 0px;
}