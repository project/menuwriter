$Id:

Menuwriter Module

Overview
--------
The Menuwriter module provides a menu block that uses dynamically generated images for menu items.  It does this by replacing the text of a menu item with an image generated from a TrueType font file which you upload to your server.

Intended Use
------------
Menuwriter is intended for sites where look is more imporatant than lightning download speeds and the primary menu is not really big. Because the menu items are images this adds to time needed to download the page. So keep that in mind when deciding whether to use this module. We don't reccommend using it for high traffic websites.

Features
--------
 - Menu block which will display the selected menu using the dynamically generated images.

 - Multiple profiles allow you to have different images for different
   menu items. Top level active menu item, Top level standard menu item, 2nd level active Menu item, 2nd level standard menu item, 3rd level active menu item and 3rd level standard menu item

 - Images generated can be transparent or opaque.

 - Text can be positioned within a background image.

 - Text can be left, right, or center aligned.

 - Images are cached to improve performance under high load.

 - Generated images can be gif, png, jpeg, or bmp.

 - Drop shadows can be added to the text.

Requirements
------------
Menuwriter is dependent on Signwriter,which can be downloaded here http://drupal.org/project/signwriter, and the core Menu module.
Menuwriter uses the GD Image library (http://php.net/image) which is standard on most shared hosting.

Installation
------------
  1. Copy the menuwriter folder to sites/all/modules/.
  2. Enable the module in drupal under admin/build/modules.
  3. Upload one or more true type fonts into one of the following directories:
      - drupal directory,
      - your files directory
      - your current theme's directory.

Creating a profile
------------------
To create your profiles, go to admin/settings/signwriter.
If you are using a background include that in the Top level normal menu item,


You need to create a profile for each of the Menu item types:
Top level normal menu item,
Top level hover menu item,
Top level active menu item,

2nd level normal menu item (if you have 2nd level menu items),
2nd level hover menu item (if you have 2nd level menu items),
2nd level active menu item (if you have 2nd level menu items),

3rd level normal menu item (if you have 3rd level menu items),
3rd level hover menu item (if you have 3rd level menu items),
3rd level active menu item (if you have 3rd level menu items),

4th level normal menu item (if you have 4th level menu items),
4th level hover menu item (if you have 4th level menu items),
4th level active menu item (if you have 4th level menu items),

Then go to admin/settings/menuwriter-block and assign the profiles to the menu items.

Enabling the block
------------------
Enable Menuwriter Block
Go to admin/build/block
Assign the block to one of the regions and save
Select the menu you want by going into configure block and select menu to show from the drop down menu.

CSS file
-----------------
The CSS file is dynamicly generated from the settings in the Top level normal profile.
If you would like to format the block differently you can choose to disable the generated CSS file and include the formating in the themes CSS file.


Embedding in theme or block snippet
------------------------------------
You can embed a menuwriter menu in your theme in place of primary links with the following code.
Replace
<?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?> (Garland theme)
or
 <?php print theme('links', $primary_links); ?> (Zen Classic theme)
with
<?php print menuwriter_tree('primary-links'); ?>

Then you will need to add some code to your CSS to make it behave.

Thanks
------
Huge thanks and kudos to Justin Freeman at Agileware http://agileware.net/ who wrote the Signwriter module,
Tilman Skobowsky 'Skybow' (Germany) and Dan Morrison 'Dman' (New Zealand)